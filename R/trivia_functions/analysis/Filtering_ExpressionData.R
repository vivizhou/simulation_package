filterExpr <- function(expr) {
  #Means of original data
  GeneMeans <- rowMeans(expr)
  cutoffmean <- quantile(GeneMeans,prob=0.2)
  #sd of original data
  GeneSd <- apply(expr,1,sd)
  #cv of original data
  GeneCV <- GeneSd/GeneMeans
  cutoffcv <- quantile(GeneCV,prob=0.2)
  
  #cut off at 20% lowest means and 20% lowest CV
  expr.cutoffByMeanAndCV <- expr[(GeneMeans > cutoffmean) & (GeneCV > cutoffcv),]
  return(expr.cutoffByMeanAndCV)
}




