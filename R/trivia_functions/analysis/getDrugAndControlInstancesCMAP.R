getDrugAndControlInstancesCMAP <- function(drugName, phenoData){
  
  drug_instances <- 
    subset(phenoData, drug==drugName)
  
  control_instances <-
    phenoData[phenoData$batch %in% drug_instances$batch & phenoData$drug == "control", ]
  
  if (nrow(control_instances) > 0){
    
    control_instances$concentration <-
      0
    
  } 
  
  output <-
    rbind(drug_instances, control_instances)
  
  #output$concentration <-
  #  output$concentration*1000000
  
  #Remove outliers
  outValue <- boxplot.stats(output[output$drug == drugName, "concentration"])$out #1.5 times more than Q3 or 1.5 times less than Q1
  
  if (length(outValue > 0)) {
    batchnumber <- output[output$concentration %in% outValue,"batch"]
    output <- output[!(output$batch %in% batchnumber),]
  }
  
  return(output)
}
