getPctControl <- function(batch_diff, pct_case) {
  pct_ctrl <- rep(pct_case, 4)[1:4]
  change <- as.numeric(c(batch_diff/2, batch_diff/2, -batch_diff/2, -batch_diff/2))
  pct_ctrl <- pct_ctrl + change
  return(pct_ctrl)
}
